

package vista;


import componenteDAO.dao.AbstractDAOFactory;
import design.IMenuCartaDAO;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import to.MenuCarta;


public class IngresoMenu extends javax.swing.JFrame {

    IMenuCartaDAO MENU_DAO = AbstractDAOFactory.getDAOFactory(AbstractDAOFactory.ARCHIVO).getMenuCartaDAO();
    List<MenuCarta> vectorDeMenu = new ArrayList<>();
    ArrayList<MenuCarta> vectorRecuperado = (ArrayList<MenuCarta>)MENU_DAO.recuperarMenu();
    boolean bandera;
    Icon error;
    Icon exito;
    public IngresoMenu() {
        initComponents();
        setSize(600,520);
        bandera=false;
        exito = new ImageIcon("src\\imagen\\exito.PNG");
        error= new ImageIcon("src\\imagen\\fail.PNG");
    }
    
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        textEntrada = new javax.swing.JTextField();
        textPlato = new javax.swing.JTextField();
        textPostre = new javax.swing.JTextField();
        textBebida = new javax.swing.JTextField();
        textUndEntrada = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        botonRegistrar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        textUndPlato = new javax.swing.JTextField();
        textUndPostre = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        textPrecioEntrada = new javax.swing.JTextField();
        textPrecioPlato = new javax.swing.JTextField();
        textPrecioPostre = new javax.swing.JTextField();
        textPrecioBebida = new javax.swing.JTextField();
        botonSalir = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registro de menú");
        getContentPane().setLayout(null);

        textEntrada.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textEntradaActionPerformed(evt);
            }
        });
        getContentPane().add(textEntrada);
        textEntrada.setBounds(175, 118, 125, 31);

        textPlato.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textPlato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPlatoActionPerformed(evt);
            }
        });
        getContentPane().add(textPlato);
        textPlato.setBounds(175, 182, 125, 31);

        textPostre.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textPostre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPostreActionPerformed(evt);
            }
        });
        getContentPane().add(textPostre);
        textPostre.setBounds(175, 245, 125, 31);

        textBebida.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textBebida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textBebidaActionPerformed(evt);
            }
        });
        getContentPane().add(textBebida);
        textBebida.setBounds(180, 310, 125, 31);

        textUndEntrada.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textUndEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textUndEntradaActionPerformed(evt);
            }
        });
        getContentPane().add(textUndEntrada);
        textUndEntrada.setBounds(479, 118, 59, 31);

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("ENTRADA");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(42, 118, 99, 31);

        botonRegistrar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        botonRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/check.PNG"))); // NOI18N
        botonRegistrar.setText("REGISTRAR");
        botonRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRegistrarActionPerformed(evt);
            }
        });
        getContentPane().add(botonRegistrar);
        botonRegistrar.setBounds(42, 384, 183, 80);

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("PORCIONES");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(462, 81, 99, 31);

        textUndPlato.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textUndPlato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textUndPlatoActionPerformed(evt);
            }
        });
        getContentPane().add(textUndPlato);
        textUndPlato.setBounds(479, 182, 59, 31);

        textUndPostre.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textUndPostre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textUndPostreActionPerformed(evt);
            }
        });
        getContentPane().add(textUndPostre);
        textUndPostre.setBounds(479, 245, 59, 31);

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("PRECIO");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(337, 81, 99, 31);

        textPrecioEntrada.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textPrecioEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPrecioEntradaActionPerformed(evt);
            }
        });
        getContentPane().add(textPrecioEntrada);
        textPrecioEntrada.setBounds(357, 118, 59, 31);

        textPrecioPlato.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textPrecioPlato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPrecioPlatoActionPerformed(evt);
            }
        });
        getContentPane().add(textPrecioPlato);
        textPrecioPlato.setBounds(357, 182, 59, 31);

        textPrecioPostre.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textPrecioPostre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPrecioPostreActionPerformed(evt);
            }
        });
        getContentPane().add(textPrecioPostre);
        textPrecioPostre.setBounds(357, 245, 59, 31);

        textPrecioBebida.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        textPrecioBebida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPrecioBebidaActionPerformed(evt);
            }
        });
        getContentPane().add(textPrecioBebida);
        textPrecioBebida.setBounds(357, 307, 59, 31);

        botonSalir.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        botonSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/salir2.PNG"))); // NOI18N
        botonSalir.setText("REGRESAR");
        botonSalir.setHideActionText(true);
        botonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSalirActionPerformed(evt);
            }
        });
        getContentPane().add(botonSalir);
        botonSalir.setBounds(360, 380, 190, 80);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(30, 120, 100, 30);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("POSTRE");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 21, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel3);
        jPanel3.setBounds(30, 240, 120, 30);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("BEBIDA");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(jPanel4);
        jPanel4.setBounds(30, 310, 110, 30);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 70, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel5);
        jPanel5.setBounds(350, 80, 70, 30);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel6);
        jPanel6.setBounds(460, 80, 100, 30);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setText("PLATO PRINCIPAL");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(30, 180, 130, 30);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/cocina.jpg"))); // NOI18N
        jLabel7.setText("jLabel7");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(0, 0, 580, 510);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void textEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textEntradaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textEntradaActionPerformed

    private void textPlatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPlatoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPlatoActionPerformed

    private void textPostreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPostreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPostreActionPerformed

    private void textBebidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textBebidaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textBebidaActionPerformed

    private void textUndEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textUndEntradaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textUndEntradaActionPerformed

    private void botonRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRegistrarActionPerformed
        String entrada=textEntrada.getText();
        String plato=textPlato.getText();
        String postre=textPostre.getText();
        String bebida=textBebida.getText();
        String centrada=textUndEntrada.getText();
        String cplato=textUndPlato.getText();
        String cpostre=textUndPostre.getText();
        String pEntrada=textPrecioEntrada.getText();
        String pPlato=textPrecioPlato.getText();
        String pPostre=textPrecioPostre.getText();
        String pBebida=textPrecioBebida.getText();
        
        if(entrada.isEmpty() || plato.isEmpty() || postre.isEmpty() || bebida.isEmpty() || centrada.isEmpty() || 
           cplato.isEmpty() || cpostre.isEmpty() || pEntrada.isEmpty() || pPlato.isEmpty() || pPostre.isEmpty() || pBebida.isEmpty()){
            JOptionPane.showMessageDialog(null, "Algún campo está vacío", "", WIDTH, error);
        } else {
           
            MenuCarta nm = new MenuCarta();

            nm.setEntrada(entrada);
            nm.setPlato(plato);
            nm.setPostre(postre);
            nm.setBebida(bebida);
            nm.setCantEntrada(Integer.parseInt(centrada));
            nm.setCantPlato(Integer.parseInt(cplato));
            nm.setCantPostre(Integer.parseInt(cpostre));
            nm.setPrecioEntrada(Double.parseDouble(pEntrada));
            nm.setPrecioPlato(Double.parseDouble(pPlato));
            nm.setPrecioPostre(Double.parseDouble(pPostre));
            nm.setPrecioBebida(Double.parseDouble(pBebida));
            if (vectorRecuperado!=null){
                vectorRecuperado.add(nm);
                vectorDeMenu.add(nm);
            }
            else
               vectorDeMenu.add(nm); 
            
            
        }
        textEntrada.setText("");
        textPlato.setText("");
        textPostre.setText("");
        textBebida.setText("");
        textUndEntrada.setText("");
        textUndPlato.setText("");
        textUndPostre.setText("");
        textPrecioEntrada.setText("");
        textPrecioPlato.setText("");
        textPrecioPostre.setText("");
        textPrecioBebida.setText("");
        
    }//GEN-LAST:event_botonRegistrarActionPerformed

    private void textUndPlatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textUndPlatoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textUndPlatoActionPerformed

    private void textUndPostreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textUndPostreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textUndPostreActionPerformed

    private void textPrecioEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPrecioEntradaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPrecioEntradaActionPerformed

    private void textPrecioPlatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPrecioPlatoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPrecioPlatoActionPerformed

    private void textPrecioPostreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPrecioPostreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPrecioPostreActionPerformed

    private void textPrecioBebidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPrecioBebidaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPrecioBebidaActionPerformed

    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSalirActionPerformed
        if(vectorDeMenu.isEmpty())
            JOptionPane.showMessageDialog(null, "No ha registrado ningún menú");
        else{
            if (vectorRecuperado!=null) {
                if (MENU_DAO.insertarMenu(vectorRecuperado)) 
                JOptionPane.showMessageDialog(null, "Registro con éxito","",WIDTH,exito);  
            }
            else{
                if (MENU_DAO.insertarMenu(vectorDeMenu)) 
                JOptionPane.showMessageDialog(null, "Registro con éxito", "",WIDTH,exito);  
            }
              
        }
        this.setVisible(false);
    }//GEN-LAST:event_botonSalirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IngresoMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IngresoMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IngresoMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IngresoMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IngresoMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonRegistrar;
    private javax.swing.JToggleButton botonSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JTextField textBebida;
    private javax.swing.JTextField textEntrada;
    private javax.swing.JTextField textPlato;
    private javax.swing.JTextField textPostre;
    private javax.swing.JTextField textPrecioBebida;
    private javax.swing.JTextField textPrecioEntrada;
    private javax.swing.JTextField textPrecioPlato;
    private javax.swing.JTextField textPrecioPostre;
    private javax.swing.JTextField textUndEntrada;
    private javax.swing.JTextField textUndPlato;
    private javax.swing.JTextField textUndPostre;
    // End of variables declaration//GEN-END:variables
}
