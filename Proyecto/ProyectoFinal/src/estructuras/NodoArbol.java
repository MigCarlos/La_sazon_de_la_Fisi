/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras;

/**
 *
 * @author usuario
 * @param <T>
 */
public class NodoArbol<T extends Comparable>{
    private T valor;
    private NodoArbol izdo;
    private NodoArbol dcho;

    public NodoArbol(T valor) {
        this.valor = valor;
    }

    public T valorNodo() {
        return valor;
    }

    public NodoArbol subarbolIzdo() {
        return izdo;
    }

    public NodoArbol subarbolDcho() {
        return dcho;
    }

    public void nuevoValor(T d) {
        valor = d;
    }

    public void ramaIzdo(NodoArbol n) {
        izdo = n;
    }

    public void ramaDcho(NodoArbol n) {
        dcho = n;
    }

}
