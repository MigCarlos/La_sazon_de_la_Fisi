/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras;


import java.util.Comparator;


/**
 *
 * @author usuario
 * @param <T>
 */
public class ArbolBusquedaBinaria<T extends Comparable> {
    NodoArbol<T> raiz;
    Lista<T> nuevo ;

    public ArbolBusquedaBinaria() {
        raiz=null;
        nuevo = new Lista<>();
    }

    public NodoArbol<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoArbol<T> raiz) {
        this.raiz = raiz;
    }

    public Comparator getComp() {
        return comp;
    }

  public void setComp(Comparator comp) {
        this.comp = comp;
    }
    
    
    
   Comparator comp = new Comparator<T>() {
        @Override
        public int compare(T o1, T o2) {
            return o1.compareTo(o2);
        }
    };
    
    public boolean insertar(T dato){
        NodoArbol<T> nuevo= new NodoArbol(dato);
        NodoArbol<T> aux= raiz;
        NodoArbol<T> b=null;
        boolean bandera=false;
        if(raiz==null){
            raiz=nuevo;
            System.out.println("en insertar"+nuevo.valorNodo().toString());
        }
        else{
            while(aux!=null){
               
                if(dato.compareTo(aux.valorNodo())<0 ){
                    System.out.println("en insertar"+nuevo.valorNodo().toString());
                    b=aux;
                    aux=aux.subarbolIzdo();
                    bandera=true;
                }
                else{
                  System.out.println("en insertar"+nuevo.valorNodo().toString());
                    b=aux;
                    aux=aux.subarbolDcho();
                    bandera=false;
                }
            }
            if(bandera){
                b.ramaIzdo(nuevo);
            }
            else{
                b.ramaDcho(nuevo);
            }
        }
        return true;
    }
    
    public void ordenarInorden(NodoArbol<T> padre){
        if(padre!=null){
            ordenarInorden((NodoArbol)padre.subarbolIzdo());
            nuevo.insertarFinal(padre.valorNodo());
            ordenarInorden((NodoArbol)padre.subarbolDcho());  
        }
    }
    
      
    public Lista<T> devolverOrdenadoInorden(){
       return nuevo;
    }

}
