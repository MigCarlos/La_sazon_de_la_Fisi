/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras;


/**
 *
 * @author usuario
 * @param <T>
 */
public class Lista <T extends Comparable>{
    Nodo<T> cabecera;

    public Nodo<T> getCabecera() {
        return cabecera;
    }

    public void setCabecera(Nodo<T> cabecera) {
        this.cabecera = cabecera;
    }
    
    public Lista(){
        cabecera=null;
    }
    
    public void insertarFinal(T dato){
        Nodo<T> nuevo, aux;
        nuevo = new Nodo();
        nuevo.setDato(dato);
        nuevo.setPrioridad(1);
        nuevo.setSiguiente(null);
        if(cabecera==null)
            cabecera=nuevo;
        else{
            aux=cabecera;
            while(aux.getSiguiente()!=null)
                aux=aux.getSiguiente();
            aux.setSiguiente(nuevo);
        } 
    }
    
    public Nodo<T> eliminarFinal(){
        Nodo<T> aux, aux2, nue;
        aux=cabecera;
        aux2=cabecera;
        nue=new Nodo<>();
        nue=null;
        if(aux==null){
            return nue;
        }
        else{
            while(aux.getSiguiente()!=null){
                aux2=aux;
                aux=aux.getSiguiente();
             }
            aux2.setSiguiente(null);
        }
        return aux;
    }
    
    public void EliminarPosicion(int pos){
        Nodo<T> p;
        int m=1;
        p = cabecera;
            
        if(pos==1){
            cabecera=p.getSiguiente();
        }else{
            while(m!=(pos-1) && p.getSiguiente()!=null){  
                p=p.getSiguiente();
                m++;
            }
            if(p.getSiguiente()!=null){
                p.setSiguiente(p.getSiguiente().getSiguiente());
            }
        }
    }
    
    
}
