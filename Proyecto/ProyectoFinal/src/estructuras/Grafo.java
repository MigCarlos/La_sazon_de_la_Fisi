/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pamela
 */
public class Grafo {
    int numNodos;
    public int matriz[][][];
    String listaNodo[];
    
    public Grafo(int num){
        numNodos = num;
        listaNodo = new String[numNodos];
        matriz = new int[numNodos][numNodos][2];
        
        for (int i = 0; i < numNodos; i++) {
            for (int j = 0; j < numNodos; j++) {
                matriz[i][j][0]=0;
            }
        }
    }
    
    public void establecerNodos() throws FileNotFoundException{
        try {
            int i=0;
            String lectura;
            FileReader f = null;
            f = new FileReader("src\\archivo\\ListaDeliveryNOBORRAR.txt");
            BufferedReader b = new BufferedReader(f);
            while((lectura = b.readLine())!=null || i==12) {
                listaNodo[i]=lectura;
                i++;
            }
        } catch (IOException ex) {
            Logger.getLogger(Grafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anñadirRutas() throws FileNotFoundException, IOException{
        boolean flag=false;
        String lectura;
        FileReader f=null;
        f=new FileReader("src\\archivo\\ListaRelacionesNOBORRAR.txt");
        BufferedReader b = new BufferedReader(f);
        
        String[] obj= new String[3];
        
        while((lectura=b.readLine())!=null){
            StringTokenizer aux = new StringTokenizer(lectura,"::");
            int j=0;
            if(aux.hasMoreTokens()){
                while(aux.hasMoreTokens()){
                    obj[j]=aux.nextToken();
                    flag=true;
                    j++;
                }
            }else
                flag=false;
            if(flag){
                establecerRelaciones(listaNodo[Integer.parseInt(obj[0])],listaNodo[Integer.parseInt(obj[1])], Integer.parseInt(obj[2]));
            }
        }
    }
    
    private void establecerRelaciones(String a, String b, int distancia){
        int pos_a = buscarNodo(a);
        int pos_b = buscarNodo(b);
        
        if(pos_a!=-1 && pos_b!=-1){
            matriz[pos_a][pos_b][0]=distancia;
            matriz[pos_b][pos_a][0]=distancia;
            matriz[pos_a][pos_b][1]=pos_a;
            matriz[pos_b][pos_a][1]=pos_b;
            
        }
    }
    
    private int buscarNodo(String nombre){
        int pos=-1;
        for (int i = 0; i < numNodos; i++) {
            if(listaNodo[i].equals(nombre))
                pos=i;
        }
        return pos;
    }
    
    public void calcularRutasMinimas(){
        for (int i = 0; i < numNodos; i++) {
            for (int j = 0; j < numNodos; j++) {
                for (int k = 0; k < numNodos; k++) {
                    if(matriz[i][j][0]>matriz[i][k][0]+matriz[k][j][0]){
                        matriz[i][j][0]=matriz[i][k][0]+matriz[k][j][0];
                        matriz[i][j][1]=k;
                    }
                }
            }
        }
    }
    
    public String mostrarCamino(int pos_a, int pos_b) {
        if (pos_a == pos_b) {
                cam = "::" + listaNodo[pos_a];
        } else {
        cam = mostrarCamino(pos_a, matriz[pos_a][pos_b][1]) + "::"+ listaNodo[pos_b];
        }
        return cam;
    } String cam;
    
    
}

