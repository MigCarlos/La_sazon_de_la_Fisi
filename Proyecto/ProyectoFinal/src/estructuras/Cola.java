/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras;

/**
 *
 * @author usuario
 * @param <T>
 */
public class Cola<T extends Comparable> {
    Nodo<T> cabecera;

    public Nodo<T> getCabecera() {
        return cabecera;
    }
    
    public Cola(){
        cabecera=null;
    }
    
    private void insertarInicio(Nodo nuevo){
        nuevo.setSiguiente(cabecera);
        cabecera=nuevo;
    }
    
    private void insertarFinal(Nodo nuevo){
        Nodo<T> aux;
        aux=cabecera;
        while(aux.getSiguiente()!=null)
            aux=aux.getSiguiente();
        aux.setSiguiente(nuevo);
    }
    
    public void insertar(T dato, int prioridad){
        Nodo<T> nuevo;
        nuevo=new Nodo();
        nuevo.setDato(dato);
        nuevo.setPrioridad(prioridad);
        nuevo.setSiguiente(null);
        if(cabecera==null)
            cabecera=nuevo;
        else{
            if(prioridad==2)
                insertarInicio(nuevo);
            else
                insertarFinal(nuevo);
        }
    }
    public Nodo<T> eliminarInicio(){
        Nodo<T> aux = null;
        if(cabecera!=null){
            aux=cabecera;
            cabecera = cabecera.getSiguiente();
        }
        return aux;
    }
    
}

