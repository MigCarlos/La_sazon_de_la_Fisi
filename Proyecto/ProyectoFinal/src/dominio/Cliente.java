/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import componenteDAO.component.ArchivoCPresencialDAO;
import componenteDAO.dao.ArchivoDAOFactory;
import estructuras.Lista;
import estructuras.Nodo;
import to.CPresencial;


public class Cliente  {
    private String nombre;
    private String dni;
    private int codigo;
    ArchivoCPresencialDAO clienteP;
    ArchivoDAOFactory fabrica;
    ArchivoCPresencialDAO g1;
    Lista<CPresencial> P;

    public Cliente() {
        fabrica=new ArchivoDAOFactory();
        clienteP=fabrica.getCPresencialDAO();
    }

 
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
        
    
public void ingresarClientePresencial(String nom, String dni){
        int indice=clienteP.devolverNumCliente();
        clienteP.insertarNumCliente();
        clienteP.insertarNumBoleta();
        CPresencial c1=new CPresencial();
        c1.setNombre(nom);
        c1.setCodigo(indice);
        c1.setDni(dni);
        clienteP.insertarCliente(c1);
    }

    
 public Nodo<CPresencial> generarOtroPedido(){
        Nodo<CPresencial> aux;
        g1=fabrica.getCPresencialDAO();
        P=g1.devolverClientes();
        aux=P.getCabecera();
        
        return aux;
    }

}
