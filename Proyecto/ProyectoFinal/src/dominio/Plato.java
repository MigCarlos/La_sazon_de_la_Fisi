/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

/**
 *
 * @author Joselyn 
 */
public class Plato {
    private String entrada;
    private String plato;
    private String postre;
    private String bebida;
    private double total;
    
    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }
    
    public void setPlato(String plato) {
        this.plato = plato;
    }

    public String getPostre() {
        return postre;
    }

    public void setPostre(String postre) {
        this.postre = postre;
    }
    
    public String getPlato(){
        return plato;
    }
    public String getBebida() {
        return bebida;
    }

    public void setBebida(String bebida) {
        this.bebida = bebida;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return ""+getEntrada()+"::"+ getPlato()+ "::"+getPostre()+ ":: "+ getBebida()+ "::"+ getTotal()+ "\n";
    }
}
