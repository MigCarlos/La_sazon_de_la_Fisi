/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

/**
 *
 * @author Joselyn 
 */
public class Mesa {
    int numMesa;

    public int getNumMesa() {
        return numMesa;
    }

    public void setNumMesa(int numMesa) {
        this.numMesa = numMesa;
    }
    
    @Override
    public String toString() {
        return "Numero de mesa: " + getNumMesa()+"}\n";
    }
    
}
