/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import componenteDAO.component.ArchivoBoletaDAO;
import componenteDAO.component.ArchivoCDeliveryDAO;
import componenteDAO.dao.ArchivoDAOFactory;
import estructuras.ArbolBusquedaBinaria;
import estructuras.Lista;
import estructuras.Nodo;
import to.Boleta;
import to.CDelivery;



public class Cajero {
    ArchivoDAOFactory fabrica;
    ArchivoCDeliveryDAO clienteD;
    ArchivoBoletaDAO g3;
    ArbolBusquedaBinaria<Boleta> abb;
    Lista<Boleta> B;
    Boleta nueva;
    ArchivoCDeliveryDAO g2;
    Lista<CDelivery> Q;
    

    public Cajero() {
        fabrica=new ArchivoDAOFactory();
        clienteD=fabrica.getCDeliveryDAO();
        nueva=new Boleta();
    }
    
   
    public Nodo<Boleta> generarReporte(){
        Boleta b=new Boleta();
        Nodo<Boleta> aux;
        g3= fabrica.getBoletaDAO();
        abb=g3.devolverBoleta();
        abb.ordenarInorden(abb.getRaiz());
        B=abb.devolverOrdenadoInorden();
        aux=B.getCabecera();
        
        return aux;
    }
    
    
}
