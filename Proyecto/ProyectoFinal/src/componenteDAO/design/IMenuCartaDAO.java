/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package design;

import java.util.List;
import to.MenuCarta;

public interface IMenuCartaDAO {
    List<MenuCarta> listarMenu();
    //Métodos porque los quiere el cliente
    
    boolean insertarMenu(List<MenuCarta> nuevoMenu);
    boolean actualizarMenu(MenuCarta actualizable);
    boolean eliminarMenu(Integer idMenu);
    boolean buscarPorId(Integer ideMenu);
    List<MenuCarta> recuperarMenu();
}

