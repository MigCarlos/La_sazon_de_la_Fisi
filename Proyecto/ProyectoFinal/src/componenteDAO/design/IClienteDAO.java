/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package design;

import estructuras.Lista;
import to.CDelivery;
import to.CPresencial;




public interface IClienteDAO {
    //List<CPresencial> listarCPresencial();
    
    void insertarCliente(Object nuevoT);
    void buscarPorCodigo(Integer ideCodigo);
    Lista<CPresencial> devolverClientes();
    Lista<CDelivery> devolverClientesDelivery();
    void insertarNumCliente();
    int devolverNumCliente();
    void insertarNumBoleta();
    int devolverNumBoleta();
     
    
    
}


