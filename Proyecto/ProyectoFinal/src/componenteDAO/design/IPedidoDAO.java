/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package design;

import estructuras.Cola;
import estructuras.Lista;
import to.Pedido;

/**
 *
 * @author Pamela
 */
public interface IPedidoDAO {
    
    boolean insertarPedido(Object nuevoPedido);
    Lista<Pedido> recuperarPedido();
    Cola<Pedido> recuperarPedido2();
    
    
}

