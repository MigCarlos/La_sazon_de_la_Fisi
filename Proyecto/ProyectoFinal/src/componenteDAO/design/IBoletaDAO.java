/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package design;

import estructuras.ArbolBusquedaBinaria;
import to.Boleta;

/**
 *
 * @author Joselyn CCanto
 */
public interface IBoletaDAO {
    
    
   void insertarBoleta(Object nuevaB);
   ArbolBusquedaBinaria<Boleta> devolverBoleta();
    
}
