/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componenteDAO.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;


public class ArchivoAcceso {
    
    public ArchivoAcceso(){
        crearArchivoMenuCarta();
        crearArchivoCPresencial();
        crearArchivoCDelivery();
        crearArchivoNumClientes();
        crearArchivoNumBoleta();
        crearArchivoPedido();
        crearArchivoPedidoPreparado();
        crearArchivoBoleta();
        
    }
    
    public void crearArchivoMenuCarta(){  
        
        
        File arch = new File("src\\archivo\\MenuCarta.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\MenuCarta.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    }
    
    public void crearArchivoCPresencial(){  
        
        
        File arch = new File("src\\archivo\\CPresencial.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\CPresencial.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    }
     
    public void crearArchivoPedido(){  
        
        
        File arch = new File("src\\archivo\\Pedido.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\Pedido.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    }
    
    public void crearArchivoNumClientes(){  
        
        
        File arch = new File("src\\archivo\\NumCliente.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\NumCliente.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    }
      
    public void crearArchivoCDelivery(){  
        
        
        File arch = new File("src\\archivo\\CDelivery.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\CDelivery.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    }
     
    public void crearArchivoNumBoleta(){  
        
        
        File arch = new File("src\\archivo\\NumBoleta.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\NumBoleta.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    } 
    
    public void crearArchivoBoleta(){  
        
        
        File arch = new File("src\\archivo\\Boleta.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\Boleta.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    }
    
    public void crearArchivoPedidoPreparado(){  
        File arch = new File("src\\archivo\\PedidoEntregable.txt");
        if(!arch.exists()){
            try{
            FileOutputStream stream= new FileOutputStream("src\\archivo\\PedidoEntregable.txt");
            ObjectOutputStream salida = new ObjectOutputStream(stream);
            
            salida.flush();
            stream.close();
            }catch(IOException e){
                JOptionPane.showMessageDialog(null, "Error de E/S de archivo", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);

            }                   
        }    
    }
}
