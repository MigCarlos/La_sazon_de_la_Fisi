/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componenteDAO.dao;

import componenteDAO.component.ArchivoBoletaDAO;
import componenteDAO.component.ArchivoCDeliveryDAO;
import componenteDAO.component.ArchivoCPresencialDAO;
import componenteDAO.component.ArchivoMenuCartaDAO;
import componenteDAO.component.ArchivoPedidoDAO;
import componenteDAO.component.ArchivoPedidoEntregableDAO;



public class ArchivoDAOFactory extends AbstractDAOFactory{

    @Override
    public ArchivoMenuCartaDAO getMenuCartaDAO() {
        return new ArchivoMenuCartaDAO();
    }
    
    @Override
    public ArchivoCPresencialDAO getCPresencialDAO() {
        return new ArchivoCPresencialDAO();
    }
    
    @Override
    public ArchivoCDeliveryDAO getCDeliveryDAO() {
        return new ArchivoCDeliveryDAO();
    }
    
    @Override
    public ArchivoPedidoDAO getPedidoDAO() {
        return new ArchivoPedidoDAO();
    }

    @Override
    public ArchivoBoletaDAO getBoletaDAO() {
        return new ArchivoBoletaDAO();
    }
    
    @Override
    public ArchivoPedidoEntregableDAO getPedidoEntregableDAO() {
        return new ArchivoPedidoEntregableDAO();
    }
}
