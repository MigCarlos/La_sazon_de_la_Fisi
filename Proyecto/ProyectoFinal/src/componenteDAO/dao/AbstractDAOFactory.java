/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componenteDAO.dao;

import design.IBoletaDAO;
import design.IClienteDAO;
import design.IMenuCartaDAO;
import design.IPedidoDAO;


public abstract class AbstractDAOFactory {
    public static final int ARCHIVO = 1;
    
    public static AbstractDAOFactory getDAOFactory(int op){
        switch (op) {             
            case ARCHIVO:
                return new ArchivoDAOFactory();    
            default: return null;
        }
    }
    
    //Todos los DAO Productos
    public abstract IMenuCartaDAO getMenuCartaDAO();               
    public abstract IClienteDAO getCPresencialDAO();
    public abstract IClienteDAO getCDeliveryDAO();
    public abstract IPedidoDAO getPedidoDAO();
    public abstract IBoletaDAO getBoletaDAO();
    public abstract IPedidoDAO getPedidoEntregableDAO();
}

