/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package to;

import java.io.Serializable;


public class MenuCarta  implements Serializable {
     private int idEntrada;
    private int idPlato;
    private int idPostre;
    private int idBebida;
    private String entrada;
    private String plato;
    private String postre;
    private String bebida;
    private int cantEntrada;
    private int cantPlato;
    private int cantPostre;
    private double precioEntrada;
    private double precioPlato;
    private double precioPostre;
    private double precioBebida;


    public int getIdEntrada() {
        return idEntrada;
    }

    public void setIdEntrada(int idEntrada) {
        this.idEntrada = idEntrada;
    }

    public int getIdPlato() {
        return idPlato;
    }

    public void setIdPlato(int idPlato) {
        this.idPlato = idPlato;
    }

    public int getIdPostre() {
        return idPostre;
    }

    public void setIdPostre(int idPostre) {
        this.idPostre = idPostre;
    }

    public int getIdBebida() {
        return idBebida;
    }

    public void setIdBebida(int idBebida) {
        this.idBebida = idBebida;
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getPlato() {
        return plato;
    }

    public void setPlato(String plato) {
        this.plato = plato;
    }

    public String getPostre() {
        return postre;
    }

    public void setPostre(String postre) {
        this.postre = postre;
    }

    public String getBebida() {
        return bebida;
    }

    public void setBebida(String bebida) {
        this.bebida = bebida;
    }

    public int getCantEntrada() {
        return cantEntrada;
    }

    public void setCantEntrada(int cantEntrada) {
        this.cantEntrada = cantEntrada;
    }

    public int getCantPlato() {
        return cantPlato;
    }

    public void setCantPlato(int cantPlato) {
        this.cantPlato = cantPlato;
    }

    public int getCantPostre() {
        return cantPostre;
    }

    public void setCantPostre(int cantPostre) {
        this.cantPostre = cantPostre;
    }

    public double getPrecioEntrada() {
        return precioEntrada;
    }

    public void setPrecioEntrada(double precioEntrada) {
        this.precioEntrada = precioEntrada;
    }

    public double getPrecioPlato() {
        return precioPlato;
    }

    public void setPrecioPlato(double precioPlato) {
        this.precioPlato = precioPlato;
    }

    public double getPrecioPostre() {
        return precioPostre;
    }

    public void setPrecioPostre(double precioPostre) {
        this.precioPostre = precioPostre;
    }

    public double getPrecioBebida() {
        return precioBebida;
    }

    public void setPrecioBebida(double precioBebida) {
        this.precioBebida = precioBebida;
    }
    
    
}
