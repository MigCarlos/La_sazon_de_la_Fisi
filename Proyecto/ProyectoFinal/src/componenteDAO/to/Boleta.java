/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package to;

import java.io.Serializable;


public class Boleta implements Serializable, Comparable<Boleta> {

    Integer numBoleta;
    String nomCliente;
    String dniCliente;
    Double total;

    public Boleta() {
        
    }

    @Override
    public int compareTo(Boleta o) {
        return this.numBoleta.compareTo(o.numBoleta);
    }
   
    
    public int getNumBoleta() {
        return numBoleta;
    }

    public void setNumBoleta(int numBoleta) {
        this.numBoleta = numBoleta;
    }

    public String getNomCliente() {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }

    public String getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(String dniCliente) {
        this.dniCliente = dniCliente;
    }

   
    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
    
    @Override
    public String toString() {
        return getNumBoleta()+"::"+getNomCliente() + "::"+getDniCliente()+"::"+getTotal()+"\n";
    }

  
    
    
    

}
