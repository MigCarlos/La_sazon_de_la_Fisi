/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package to;

import java.io.Serializable;
import dominio.Plato;

/**
 *
 * @author Joselyn 
 */
public class Pedido implements Serializable, Comparable {
    private int  codCliente;
//    Mesa mesa;
    
    private Plato plato;
    
    

//    public Mesa getMesa() {
//        return mesa;
//    }
//
//    public void setMesa(Mesa mesa) {
//        this.mesa = mesa;
//    }

    
    @Override
    public String toString() {
        return "" + getCodCliente() + "::" + getPlato().getEntrada() + "::" + 
                    getPlato().getPlato() + "::" + getPlato().getPostre() + "::" + getPlato().getBebida() + "::" +
                    getPlato().getTotal() + "\n";
    }

    
    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Plato getPlato() {
        return plato;
    }

    public void setPlato(Plato plato) {
        this.plato = plato;
    }

    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }
    
}
