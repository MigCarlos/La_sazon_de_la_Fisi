/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componenteDAO.component;

import componenteDAO.dao.Gestor;
import design.IPedidoDAO;
import estructuras.Cola;
import estructuras.Lista;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import dominio.Plato;
import to.Pedido;

/**
 *
 * @author MiguelSC
 */
public class ArchivoPedidoEntregableDAO extends Gestor implements IPedidoDAO {
    Lista<Pedido> pedido;
    
    public ArchivoPedidoEntregableDAO() {
        pedido=new Lista<>();
    }
    
    @Override
    public boolean insertarPedido(Object nuevoPedido) {
        FileWriter fichero = null;
        PrintWriter pw = null;
                    try
                    {
                        int can1=0;
                        System.out.println(recorridoArchivo("src\\archivo\\PedidoEntregable.txt"));
                        if(recorridoArchivo("src\\archivo\\PedidoEntregable.txt")==1 &&can1<1){
                            fichero = new FileWriter("src\\archivo\\PedidoEntregable.txt");
                            can1++;
                            
                        }
                        else{
                            fichero = new FileWriter("src\\archivo\\PedidoEntregable.txt",true);
                        }
                        pw = new PrintWriter(fichero);
                        pw.println(nuevoPedido);
                        

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                       try {
                       if (null != fichero)
                          fichero.close();
                       } catch (Exception e2) {
                          e2.printStackTrace();
                       }
                    }
        
        return true;
    }
    
    public int recorridoArchivo(String ruta){
        int contador = 0;
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File (ruta);
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);

            String linea;
            while((linea =br.readLine())!=null)
                contador++;  
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
         
            try{                    
                if( null != fr )  
                fr.close();                       
            }catch (Exception e2){ 
                e2.printStackTrace();
            }
        }
        return contador;
    }

    @Override
    public Lista<Pedido> recuperarPedido() {
        File archivo;
        FileReader fr = null;
        BufferedReader br;
        boolean flag=false;
 
        try {
            archivo = new File ("src\\archivo\\PedidoEntregable.txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
                        
            
            String linea;
                        
            String[] obj= new String[6];
            Pedido nuevoPedido;
            Plato nuevoPlato;
            while((linea=br.readLine())!=null){
                 
                nuevoPedido=new Pedido();
                nuevoPlato=new Plato();
                StringTokenizer aux = new StringTokenizer(linea,"::");
                int j=0;
                if(aux.hasMoreTokens()){
                    while(aux.hasMoreTokens()){
                        obj[j]=aux.nextToken();
                        flag=true;
                        j++;
                    }
                }else{
                    flag=false;
                }
                if(flag){
                    nuevoPedido.setCodCliente(Integer.parseInt(obj[0]));
                    nuevoPlato.setEntrada(obj[1]);
                    nuevoPlato.setPlato(obj[2]);
                    nuevoPlato.setPostre(obj[3]);
                    nuevoPlato.setBebida(obj[4]);
                    nuevoPlato.setTotal(Double.parseDouble(obj[5]));
                    nuevoPedido.setPlato(nuevoPlato);
                    pedido.insertarFinal(nuevoPedido);
                }
                
            }
                br.close();
                fr.close();
            }      
            catch(IOException e){
            }
            finally{
                try{
                    if( null != fr ){
                        fr.close();
                    }
                }catch (IOException e2){}
            }

        return pedido;
    }
    
    
    @Override
    public Cola<Pedido> recuperarPedido2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
