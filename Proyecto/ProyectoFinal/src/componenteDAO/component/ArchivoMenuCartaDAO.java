/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componenteDAO.component;

import componenteDAO.dao.ArchivoAcceso;
import componenteDAO.dao.Gestor;
import design.IMenuCartaDAO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import to.MenuCarta;


public class ArchivoMenuCartaDAO extends Gestor implements IMenuCartaDAO {

    ArchivoAcceso ARCHIVO_ACCESO = new ArchivoAcceso();
    @Override
    public List<MenuCarta> listarMenu() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insertarMenu(List<MenuCarta> nuevoMenu) {
        boolean guardado=false;
        ObjectOutputStream menu;
        try {
            FileOutputStream ostream = new FileOutputStream("menu.txt");
            menu = new ObjectOutputStream(ostream);
            menu.writeObject(nuevoMenu);
            guardado=true;
            menu.close();
        } catch (IOException ex) {
            Logger.getLogger(ArchivoMenuCartaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return guardado;
    }
    
    
    @Override
    public List<MenuCarta> recuperarMenu() {
        File arch = new File("menu.txt");
        List<MenuCarta> list=null;
        if(arch.exists()){
            try {
            FileInputStream istream = new FileInputStream("menu.txt");
            ObjectInputStream entrada = new ObjectInputStream(istream);
            list = (ArrayList<MenuCarta>)entrada.readObject();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ArchivoMenuCartaDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(ArchivoMenuCartaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            return null;
        }
        return list;
    }
    
    @Override
    public boolean actualizarMenu(MenuCarta actualizable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminarMenu(Integer idMenu) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean buscarPorId(Integer ideMenu) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
