/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componenteDAO.component;

import componenteDAO.dao.Gestor;
import design.IBoletaDAO;
import estructuras.ArbolBusquedaBinaria;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.StringTokenizer;
import to.Boleta;

/**
 *
 * @author Joselyn CCanto
 */
public class ArchivoBoletaDAO extends Gestor implements IBoletaDAO {

    ArbolBusquedaBinaria<Boleta> abb;
    

    public ArchivoBoletaDAO() {
        abb=new ArbolBusquedaBinaria<>();
       
    }
    
    
    
    
    @Override
    public void insertarBoleta(Object nuevaB) {
         FileWriter fichero = null;
                    PrintWriter pw = null;
                    try
                    {
                        int can1=0;
                        if(recorridoArchivo()==1 &&can1<1){
                            fichero = new FileWriter("src\\archivo\\Boleta.txt");
                            can1++;                         
                        }
                        else{
                            fichero = new FileWriter("src\\archivo\\Boleta.txt",true);
                        }
                        pw = new PrintWriter(fichero);
                        pw.println(nuevaB);

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                       try {
                        if (null != fichero)
                           fichero.close();
                       } catch (Exception e2) {
                          e2.printStackTrace();
                       }
                    }
                    
    }

    public int recorridoArchivo(){
     int contador = 0;
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;

      try {
         archivo = new File ("src\\archivo\\Boleta.txt");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

      
         String linea;
          while((linea =br.readLine())!=null){
              contador++;             
            
         }
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
        return contador;
    }
    
    
    @Override
    public ArbolBusquedaBinaria<Boleta> devolverBoleta() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        boolean flag=false;
 
        try {
            archivo = new File ("src\\archivo\\Boleta.txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
                        
            
            String linea;
                        
            String[] obj= new String[4];
            Boleta nuevo;
            while((linea=br.readLine())!=null){
                nuevo=new Boleta();
                StringTokenizer aux = new StringTokenizer(linea,"::");
                int j=0;
                if(aux.hasMoreTokens()){
                    while(aux.hasMoreTokens()){
                        obj[j]=aux.nextToken();
                        flag=true;
                        j++;
                    }
                }else{
                    flag=false;
                }
                if(flag){
                    nuevo.setNumBoleta(Integer.parseInt(obj[0]));
                    nuevo.setNomCliente(obj[1]);
                    nuevo.setDniCliente(obj[2]);
                    nuevo.setTotal(Double.parseDouble(obj[3]));
                     System.out.println("nuevo" +nuevo.toString());
                     Comparator<Boleta> bole;
                         bole = new Comparator<Boleta>() {
                         @Override
                            public int compare(Boleta b1, Boleta b2) {
                                Integer boleta1 = b1.getNumBoleta();
                                Integer boleta2 = b2.getNumBoleta();
                                return boleta1.compareTo(boleta2);
                                
                            }
                         };
                         abb.setComp(bole);
                         abb.insertar(nuevo);
                        
                }
                
               
            }
            
                br.close();
                fr.close();
        }      
        catch(IOException e){
        }
        finally{
            try{
                if( null != fr ){
                   fr.close();
                }
            }catch (IOException e2){
            }
        }

        return abb;
    }

    
    
}
