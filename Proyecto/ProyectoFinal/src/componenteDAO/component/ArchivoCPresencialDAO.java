/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componenteDAO.component;

import estructuras.Lista;
import componenteDAO.dao.Gestor;
import design.IClienteDAO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import to.CDelivery;
import to.CPresencial;


public class ArchivoCPresencialDAO extends Gestor implements IClienteDAO {
//Metodos abstractos
    
   Lista<CPresencial> P;
   

    public ArchivoCPresencialDAO() {
        P=new Lista<>();
    }
    
    
    
    @Override
    public void insertarCliente(Object nuevoT) {
        FileWriter fichero = null;
                    PrintWriter pw = null;
                    try
                    {
                        int can1=0;
                        System.out.println(recorridoArchivo());
                        if(recorridoArchivo()==1 &&can1<1){
                            fichero = new FileWriter("src\\archivo\\CPresencial.txt");
                            can1++;
//                            
                        }
                        else{
                            fichero = new FileWriter("src\\archivo\\CPresencial.txt",true);
                        }
                        pw = new PrintWriter(fichero);
                        pw.println(nuevoT);

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                       try {
                       if (null != fichero)
                          fichero.close();
                       } catch (Exception e2) {
                          e2.printStackTrace();
                       }
                    }
      
    }

    @Override
    public void buscarPorCodigo(Integer ideCodigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Lista<CPresencial> devolverClientes()  {
        
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        boolean flag=false;
 
        try {
            archivo = new File ("src\\archivo\\CPresencial.txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            String linea;
                        
            String[] obj= new String[3];
            CPresencial nuevo;
            while((linea=br.readLine())!=null){
//                System.out.println(linea);
                 
                nuevo=new CPresencial();
                StringTokenizer aux = new StringTokenizer(linea,"::");
                int j=0;
                if(aux.hasMoreTokens()){
                    while(aux.hasMoreTokens()){
                        obj[j]=aux.nextToken();
                        flag=true;
                        j++;
                    }
                }else{
                    flag=false;
                }
                if(flag){
                    nuevo.setCodigo(Integer.parseInt(obj[0]));
                    nuevo.setNombre(obj[1]);
                    nuevo.setDni(obj[2]);
                    P.insertarFinal(nuevo);
                }
                
                }
                br.close();
                fr.close();
            }      
            catch(IOException e){
            }
            finally{
                try{
                    if( null != fr ){
                        fr.close();
                    }
                }catch (IOException e2){}
            }

        return P;
    }
    
    public int recorridoArchivo(){
     int contador = 0;
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;

      try {
         archivo = new File ("src\\archivo\\CPresencial.txt");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

        
         String linea;
          while((linea =br.readLine())!=null){
              contador++;             
            
         }
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
        return contador;
    }

    @Override
    public Lista<CDelivery> devolverClientesDelivery(){ //No será implementado por ser Cliente Presencial
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertarNumCliente() {
        FileWriter fichero = null;
         int j=1;
                    PrintWriter pw = null;
                    try
                    {
                        int m=devolverNumCliente();
                        if(m==0){
                            fichero = new FileWriter("src\\archivo\\NumCliente.txt");
                            pw = new PrintWriter(fichero);
                            pw.println(j);
//                            
                        }
                        else{
                            fichero = new FileWriter("src\\archivo\\NumCliente.txt");
                            j=m;
                            pw = new PrintWriter(fichero);
                            pw.println(j+1);
                        }
                       

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                       try {
                       if (null != fichero)
                          fichero.close();
                       } catch (Exception e2) {
                          e2.printStackTrace();
                       }
                    }
      
    }

    @Override
    public int devolverNumCliente() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        int i = 0;
        try {
            archivo = new File ("src\\archivo\\NumCliente.txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            String linea;
            
            linea=br.readLine();
            if(esNumero(linea)){
                i=Integer.parseInt(linea);
            }
            br.close();
            fr.close();
        }      
        catch(IOException e){
        }
        finally{
            try{
                if( null != fr ){
                    fr.close();
                }
            }catch (IOException e2){}
        }

        return i;
    }

    private static boolean esNumero(String linea){
        try{
            Integer.parseInt(linea);
            return true;
        }catch(NumberFormatException e){
            return false;
        }
    }
    @Override
    public void insertarNumBoleta() {
        FileWriter fichero = null;
         int j=1;
                    PrintWriter pw = null;
                    try
                    {
                        int m=devolverNumBoleta();
                        if(m==0){
                            fichero = new FileWriter("src\\archivo\\NumBoleta.txt");
                            pw = new PrintWriter(fichero);
                            pw.println(j);
//                            
                        }
                        else{
                            fichero = new FileWriter("src\\archivo\\NumBoleta.txt");
                            j=m;
                            pw = new PrintWriter(fichero);
                            pw.println(j+1);
                        }
                       

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                       try {
                       if (null != fichero)
                          fichero.close();
                       } catch (Exception e2) {
                          e2.printStackTrace();
                       }
                    }
    }

    @Override
    public int devolverNumBoleta() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        int i = 0;
        try {
            archivo = new File ("src\\archivo\\NumBoleta.txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            String linea;
            
            linea=br.readLine();
            System.out.println("linea" +linea);
            if(esNumero(linea)){
                i=Integer.parseInt(linea);
            }
            br.close();
            fr.close();
        }      
        catch(IOException e){
        }
        finally{
            try{
                if( null != fr ){
                    fr.close();
                }
            }catch (IOException e2){}
        }

        return i;
    }
 }
    

