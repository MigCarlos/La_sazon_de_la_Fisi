/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;


public class GestorDeUsuario {

    private static int permisoUsuarioActivo;
    
    
    public boolean verificaUsuarioPassword(String codigoPersonal, String pas) {
        boolean flag=false;
        
        if(codigoPersonal.equals("1") && pas.equals("1")){
            permisoUsuarioActivo=1;
            flag=true;
        }
        else{
            if(codigoPersonal.equals("00102") && pas.equals("201")){
                permisoUsuarioActivo=2;
                flag=true;
            }
            else{
                if(codigoPersonal.equals("00103") && pas.equals("301")){
                    permisoUsuarioActivo=3;
                    flag=true;
                }
                else{
                    if(codigoPersonal.equals("00104") && pas.equals("401")){
                        permisoUsuarioActivo=4;
                        flag=true;
                   }
                }
            }
        }
        return flag;
    }
 
    
      public int retornarPermisoUsuario() {
        return permisoUsuarioActivo;
    }
}